# Kubernetes Agent Readiness Review

## Production Readiness Guide

For any new or existing system or a large feature set the questions in this guide help make the existing service more robust, and for new systems it helps prepare them and speed up the process of becoming fully production-ready.

Initially, this guide is likely to be used by Production Engineers who are embedded with other teams working on existing services and features. However, anyone working on a new service or feature set is encouraged to use this guide as well.

The goal of this guide is to help others understand how the new service or feature set may impact the rest of the (production) system; what steps need to be taken (besides deploying this new system) to ensure that it can be properly managed; and to understand what it will take to manage the reliability of the new system / feature / service beyond its' initial deployment.

## Summary

The GitLab Kubernetes Agent is an active in-cluster component for solving GitLab and Kubernetes integration tasks in a secure and cloud-native way. Please refer to https://docs.gitlab.com/ee/user/clusters/agent/ for more details.

This service is currently in market validation. We wish to increase adoption of this new feature, especially on GitLab.com (self-managed instances can probably use an [existing feature](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html), so that we can learn more about what users need, and determine next features to [build](https://gitlab.com/groups/gitlab-org/-/epics/3329).
Relevant business metrics:

- https://about.gitlab.com/handbook/product/ops-section-performance-indicators/#configureconfigure---amau---number-of-gitlab-kubernetes-agent-instances

## Architecture

See https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/architecture.md#high-level-architecture

We have two components for the Kubernetes agent:

- The GitLab Kubernetes Agent Server (`kas`). This is deployed server-side together with the GitLab web (Rails), and Gitaly. It's responsible for:
  - Accepting requests from `agentk`.
  - [Authentication of requests](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/identity_and_auth.md) from `agentk` by querying `GitLab RoR`.
  - Fetching agent's configuration from a corresponding Git repository by querying `Gitaly`.
  - Polling manifest repositories for [GitOps support](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/gitops.md) by talking to `Gitaly`.
- The GitLab Kubernetes Agent (`agentk`). This is deployed to the user's Kubernetes cluster. It is responsible for:
  - Keeping a connection established to a `kas` instance
  - Processing GitOps requests from `kas`

## Operational Risk Assessment

### Use cases

1. The user can configure an agent by committing a configuration file in a GitLab project. We refer to this project as the [configuration repository](https://docs.gitlab.com/ee/user/clusters/agent/#define-a-configuration-repository).
  1. `kas` will then periodically poll Gitaly for configuration updates for each connected agent. When there is a new commit, `kas` fetches the agent's configuration file from the configuration repository, and sends it to the corresponding agent. Agent then configures itself with the new configuration.
1. The user specifies in the configuration file which `manifest_projects` to sync. These manifest projects are other GitLab projects. The agent needs to be authorized to read the repository of a manifest project. The user then commits YAML definitions for Kubernetes resources into each manifest project.
  1. For each connected agent, `kas` polls Gitaly for the manifest repositories, and sends the latest manifests to the agent. The agent then applies the latest manifest to the Kubernetes cluster.

For the first release, note that we will only sync one file per manifest project, and this file must be named `manifest.yaml`. Additionally, only public projects are allowed as manifest projects for the first release.

### Scalability

1. We intend to use WebSockets with HAProxy intially. As the number of concurrent connections grows, the HAProxy fleet will need to be scaled accordingly.
1. When HAProxy supports gRPC, we intend to start using gRRPC. The performance and scalability on GitLab.com will need to be tested.
1. The `kas` chart is configured by default to autoscale by using a [HorizontalPodAutoscaler](https://gitlab.com/gitlab-org/charts/gitlab/-/blob/master/charts/gitlab/charts/kas/templates/hpa.yaml). The HorizontalPodAutoscaler is configured to target an average value of 100m CPU.

### Dependencies

1. Load Balancer, is used to load balance requests between the `agentk` and `kas`.
1. GitLab Web (Rails) server, which serves the internal API for `kas`.
1. Gitaly, which provides repository blobs for the agent configuration, and K8s resources to be synced.

### Operational Risks

1. Abuse of many concurrent connections.
1.

In the future, when we support syncing multiple files, we will need to keep a limit of files synced per project to mitigate abuse.

### Controls

1. The internal API for `kas` can be disabled using a feature flag (`kubernetes_agent_internal_api`).

## Database

There is no database for either the `kas`, nor the `agentk` components.

Note that we are considering Redis in the future - please see https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/90

## Security and Compliance

Please see https://gitlab.com/gitlab-com/gl-security/appsec/appsec-reviews/-/issues/30

## Performance

A rate limit on a per client basis is [planned](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/issues/25)

The frequency of gRPC calls from `kas` to `Gitaly` can be configured (see https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/kas_config_example.yaml).

## Backup and Restore

N/A as components are stateless

## Monitoring and Alerts

Monitoring is planned to be added soon - please see https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/issues/9.

Structured logging (JSON) and ELK is planned - please see https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/issues/22.

Error tracking (Sentry) is implemented in https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/issues/27

There is currently no target SLA for this service.

## Responsibility

The [Configure group](https://gitlab.com/groups/gitlab-org/configure/-/group_members?with_inherited_permissions=exclude) are the subject matter experts for this service.

`@nicholasklick`, `@tkuah`, and `@ash2k` are available to be on-call for the launch.

## Testing

The [project](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent) has extensive unit tests that are run in GitLab's CI pipelines.

Automatic GitLab QA end-to-end tests are planned in https://gitlab.com/gitlab-org/gitlab/-/issues/254870. We have [detailed manual steps for QA](https://gitlab.com/gitlab-org/charts/gitlab/-/blob/master/doc/charts/gitlab/kas/index.md#developement-how-to-manual-qa) for the chart.

There has been no load testing done on this service.

