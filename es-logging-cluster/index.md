# Elastic Search 7.x Infra Logging Readiness Review

## Table Of Contents

<!-- vim-markdown-toc GitLab -->

* [Summary](#summary)
* [Documentation](#documentation)
* [Architecture](#architecture)
  * [GitLab.com Logging Architecture](#gitlab.com-logging-architecture)
  * [Elastic Logging Cluster Architecture](#elastic-logging-cluster-architecture)
    * [Physical Architecture](#physical-architecture)
    * [Logical Architecture](#logical-architecture)
    * [Index Management](#index-management)
* [Operational Risk Assessment](#operational-risk-assessment)
  * [Affected Dependencies](#affected-dependencies)
  * [Component Failures](#component-failures)
    * [Fluentd](#fluentd)
    * [Cloud PubSub](#cloud-pubsub)
    * [Pubsubbeat](#pubsubbeat)
    * [Kibana](#kibana)
    * [Node Failure in Elastic Cluster](#node-failure-in-elastic-cluster)
    * [Availability Zone Failure](#availability-zone-failure)
    * [Internal Cluster Failures](#internal-cluster-failures)
  * [Performance Degradation](#performance-degradation)
    * [Degradation After Migration](#degradation-after-migration)
    * [Degradation Because Of Shard Imbalance](#degradation-because-of-shard-imbalance)
  * [Scalability](#scalability)
  * [Usability Changes](#usability-changes)
* [Rollback](#rollback)
* [Data and Retention](#data-and-retention)
* [Performance](#performance)
  * [Indexing performance](#indexing-performance)
  * [Query Performance](#query-performance)
* [Security](#security)
* [Backup and Restore](#backup-and-restore)
* [Monitoring and Alerts](#monitoring-and-alerts)
* [Responsibility](#responsibility)
* [Testing](#testing)
* [Readiness Review Participants](#readiness-review-participants)

<!-- vim-markdown-toc -->

## Summary

We need to migrate away from our Elastic Search 5.x elastic.io cluster to a
newer Elastic Search 7.x version for logging, mainly because there is no
support and no updates for the old version anymore. The operation of the old
cluster also isn't very stable and it never ran through a readiness review, so
this review will cover the migration as well as the general operational
readiness of our ES 7.x logging cluster.

Main goals for the migration are

* switching to a supported version
* sizing the cluster to support our growing log volume
* providing good search latencies
* improving log retention time and costs by using new features like
  [ILM](https://www.elastic.co/guide/en/elasticsearch/reference/current/index-lifecycle-management.html)
  and hot-warm architecture
* improving reliability of elastic logging as an important observability tool by
  setting up proper monitoring and alerting for the cluster

The umbrella epic for the migration is
https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/130.

## Documentation

The Elasticsearch logging cluster
[documentation](https://gitlab.com/gitlab-com/runbooks/-/tree/master/elastic/doc)
is located in the runbooks repository, including [troubleshooting
runbooks](https://gitlab.com/gitlab-com/runbooks/tree/master/elastic/doc/troubleshooting)
and tooling for many [helpful API
calls](https://gitlab.com/gitlab-com/runbooks/-/tree/master/elastic/api_calls)
as well as [configuration for managed
objects](https://gitlab.com/gitlab-com/runbooks/-/tree/master/elastic/managed-objects)
in the clusters. A CI pipeline is applying changes to those configurations
automatically.

## Architecture

### GitLab.com Logging Architecture

```mermaid
graph BT
  subgraph GCP
    subgraph gprd

      GCS[GCS bucket]

      Stackdriver

      subgraph gprdVMs[GCE VMs]
        fluentd
      end

      subgraph pubsubVMs
        pubsubbeat
      end

      subgraph PubSub
        topics
      end

      subgraph GKE[GKE pods]
        k8s
      end

    end
  end

  subgraph Elastic
    ES7_prod[ES7 prod cluster]
    ES7_monitoring[ES7 monitoring cluster]
  end

  k8s[fluentd] --> Stackdriver

  fluentd -- all logs --> Stackdriver
  fluentd -- excluding haproxy, nginx --> topics

  Stackdriver -- for 1 year --> GCS
  Stackdriver -- GKE logs --> topics

  pubsubbeat -- subscribes --> topics
  pubsubbeat -- sends logs --> ES7_prod

  ES7_prod -- xpack metrics --> ES7_monitoring

style Elastic fill:#9BF;
style PubSub fill:#1AD;
style gprdVMs fill:#3E7;
style GKE fill:#3E7;
style pubsubVMs fill:#5A6;
style GCP fill:#FFE;
```

We have **fluentd** running on all our servers which is sending selected logs to
both **Stackdriver** and **PubSub** in GCP. While **Stackdriver** is getting
*all* logs (keeping them for 30 days, a GCS bucket is used for long-term
storage), we currently exclude haproxy and nginx logs from **PubSub** because of
their volume. For each **PubSub topic** (e.g. `workhorse`. `rails`, ...) we have
one **pubsubbeat** machine pulling the logs out of **PubSub** and sending them
into Elastic.  We are not sending logs directly into Elastic, because if Elastic
isn't reachable we can't store the pending logs for a long time on the hosts and
might lose them. This also allows us to optimise the process of uploading logs
by using huge bulk requests from a small number of beats.  **PubSub** serves as
a reliable buffer and also enables us to have multiple clients subscribed to the
same logging topic. This is making the migration easy, as we can run both the
new and the old logging cluster in parallel with the same logs.

Due to relatively low requirements for monitoring clusters, all ES 7.x clusters
send their x-pack monitoring metrics to a single monitoring cluster called
**es7-monitoring**. At the moment of writing this includes:
**gitlab-logs-prod**, **gitlab-logs-nonprod** and **es7-monitoring**. Using a
dedicated monitoring cluster is also desirable in the context of production
environments (having a production cluster monitor itself wouldn't make much
sense in case of an incident).

For clusters running in Elastic Cloud, the only logs we have access to are the
ones available in the **Logs tab** in the deployment page (available at
**cloud.elastic.co**). This UI is very limited and does not allow any advanced
searching. We reached out to Elastic about it and they are aware of these
limitations and are working on providing users with better access to logs.

### Elastic Logging Cluster Architecture

#### Physical Architecture

```mermaid
graph TB
  
subgraph GCP
  subgraph us-central1_Zone1
    
    subgraph ControlNodesZ1
      MachineLearning
      Kibana
      APM
      Master1
    end

    subgraph HotNodesZ1
      highIO1
      highIO2
      highIO3[highIO...]
      highIO7
    end
    subgraph WarmNodesZ1
      highStorage1
      highStorage2
    end
  end

  subgraph us-central1_Zone2
    Master2

    subgraph HotNodesZ2
      highIO8
      highIO9
      highIO10[highIO...]
      highIO14
    end
    subgraph WarmNodesZ2
      highStorage3
      highStorage4
    end
  end

  subgraph us-central1_Zone3
    Master3
  end
end

Master1 --> highIO7
Master2 --> highIO9
Master3 --> highIO14

highIO7 -- move index after 2d --> highStorage2
highIO7 --> highStorage3
highIO7 --> highStorage1

highIO8 --> highStorage2
highIO8 --> highStorage4
highIO8 -- move index after 2d --> highStorage3

style GCP fill:#FFE;

style HotNodesZ1 fill:#F66;
style HotNodesZ2 fill:#F66;
style WarmNodesZ1 fill:#FAA;
style WarmNodesZ2 fill:#FAA;

style Master1 fill:#F44;
style Master2 fill:#F44;
style Master3 fill:#F44;

style MachineLearning fill:#AFA;
style Kibana fill:#AAF;
style APM fill:#A58;

%% make Master links invisible - just used for ordering
linkStyle 0 stroke-width:0px;
linkStyle 1 stroke-width:0px;
linkStyle 2 stroke-width:0px;
```

The new logs.gprd.gitlab.net cluster (named **gitlab-logs-prod** internally) has
it's nodes distributed in 2 GCP availability zones. It currently consists of 14
**highio nodes** (7 in each zone, 64GiB RAM, 1.88TiB storage per node) used as
**hot nodes** for log indexing and searching and 4 **high storage** nodes (2 in
each zone, 64GiB RAM, 10TiB storage per node) used as **warm nodes** for storing
and searching only.

Shards of an active index are distributed over all hot nodes. ILM is configured
to roll over to a new index after 12h or reaching 30GiB. Indices older than 2
days are moved to warm nodes (also distributed over all warm nodes for balancing
of search queries and reliability).

Additionally the cluster has 3 dedicated master instances (one per zone, to have
a quorum on cluster partition) and one instance for Kibana, APM and Machine
Learning.

The elastic cluster is deployed in the us-central1 region while we are operating
GitLab.com in us-east1. This means we are paying 0.01$/GiB log traffic which
accounts to around 14$/day. Log ingestion isn't latency-sensitive but Kibana UI
responsiveness from EMEA might be slightly affected by this.

#### Logical Architecture

```mermaid
graph TB
  User

  subgraph Elastic
    API
    Watches
    Query[Query]
    IndexPattern

    subgraph Kibana
      UI
      Dashboards
      SavedSearches
      Visualizations
    end

    subgraph AZ1
      subgraph Node1
        PrimaryShard1
        ReplicaShard2
        PrimaryShardN
      end
    end

    subgraph AZ2
      subgraph Node2
        ReplicaShard1
        PrimaryShard2
        ReplicaShardN
      end
    end

    IndexTemplate -- points to --> ILM_Policy -- manages --> Index
    IndexAlias -- uses --> IndexTemplate
    IndexAlias -- "points to" --> CurrentIndex -- "rolls over to"--> RolledOverIndex
    CurrentIndex -- is an --> Index
    RolledOverIndex -- is an --> Index

    Index --> PrimaryShard1
    Index --> ReplicaShard2
    Index --> ReplicaShardN
    Index --> ReplicaShard1
    Index --> PrimaryShard2
    Index --> PrimaryShardN
  end

  User --> API
  User --> UI

  UI --> Dashboards
  UI --> SavedSearches
  UI --> Visualizations

  Dashboards --> API
  SavedSearches --> API
  Visualizations --> API

  API -- manages --> Watches
  API -- execute --> Query
  Watches --> Slack
  Watches -- execute --> Query
  Query -- against --> IndexPattern
  IndexPattern -- matches --> CurrentIndex
  IndexPattern -- matches --> RolledOverIndex

style Elastic fill:#9BF;
style Kibana fill:#CBF;
style API fill:#CBF;
style IndexTemplate fill:#2AF;
style ILM_Policy fill:#28F;
style IndexAlias fill:#2F2;
style IndexPattern fill:#ACA;
style CurrentIndex fill:#4D4;
style RolledOverIndex fill:#6B6;
style Index fill:#191;
style Slack fill:#BFF;
style Watches fill:#B44;

style AZ1 fill:#FFE;
style AZ2 fill:#FFE;

style PrimaryShard1 fill:#D88;
style PrimaryShard2 fill:#D88;
style PrimaryShardN fill:#D88;
style ReplicaShard1 fill:#88D;
style ReplicaShard2 fill:#88D;
style ReplicaShardN fill:#88D;
```

#### Index Management

There is an **index alias** for each PubSub topic with a corresponding name
(e.g. `pubsub-rails-inf-gprd`), pointing at the currently active index for that
topic (e.g. `pubsub-rails-inf-gprd-000005`). Index Lifecycle Management (ILM) is
configured to roll-over each index if bigger than 30GiB or older than 12h and a
new active index is created, with an increasing number appended to it's name.
Rolled-over indices are moved to the warm nodes if older than 2 days. With a
targeted retention period of 7 days, indices older than that are automatically
deleted. Each index consists of multiple primary shards. We decided that logs in
Elastic are important enough to have one replica shard for each primary shard
(automatically stored on a different node for recovery in case of a
node-failure).

## Operational Risk Assessment

### Affected Dependencies

The Elastic Search logging cluster is an important observability tool for the
operation of GitLab.com. A failure of Elastic or delayed/missing logs would not
directly affect Gitlab.com availability but it would mean

* Alerts based on Elastic watches would not work anymore (abuse alerts, failure
  condition alerts, ...), leading to a higher MTTD
* our ability to search for the root cause of failures/performance
  issues/outages would be very limited, having a big impact on our MTTR
* detecting abusers would be very hard
* many Kibana visualizations and some Grafana dashboards would not work anymore
* Grafana dashboards made available to our customers would stop working

### Component Failures

#### Fluentd

If fluentd fails to send messages, we would have them locally on each host until
they are rotated. In most cases it happens after a few hours, for example rails
logs are deleted on the web nodes after ~6h. Fluentd keeps track of the last
position in each log file. The pubsub plugin isn't buffering messages locally.

We already alert when the fluentd process is down:
https://gitlab.com/gitlab-com/runbooks/blob/master/rules/fluentd.yml#L5

We will also setup alerts for fluentd failing to send messages:
https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/9054

#### Cloud PubSub

GCP PubSub has an SLO of 99.95% uptime per month. If PubSub is down, fluentd
would retry sending until it is up again. In the worst case, we still would
have logs in Stackdriver.

#### Pubsubbeat

In terms of pubsubbeat operations we are monitoring:

- Cloud PubSub subscriptions for [unacknowledged
  messages](https://gitlab.com/gitlab-com/runbooks/blob/master/rules/pubsub.yml#L7)
- pubsubbeat logs for [warnings and
  errors](https://gitlab.com/gitlab-com/runbooks/blob/master/rules/pubsubbeat.yml#L4-26)
- an internal pubsubbeat metric for [dropped
  messages](https://gitlab.com/gitlab-com/runbooks/blob/master/rules/pubsubbeat.yml#L27-37)
  (messages that the beat pulled from PubSub and marked as acknowledged, but
  failed to upload to Elastic and dropped them as a result)

If pubsubbeat fails or slows down, messages stay in PubSub topics until all
subscribers acknowledge them or after retention period passes (by default 7d).

#### Kibana

There is a single Kibana instance, managed by Elastic. In case of failure, this
instance could be restarted via UI. If re-creation is needed, Elastic.io
provides a priority 1 support response time of 4h at EMEA business times.

#### Node Failure in Elastic Cluster

As we are using one replica shard, and primary and replica shards always are
distributed on different nodes, we can survive at least one failing node. Shard
re-balancing will lead to degraded performance though - on peak load the cluster
may fall behind on indexing logs. Adding a replacement node also can lead to
reduced indexing performance because of initial
[shard-imbalance](https://gitlab.com/gitlab-com/runbooks/tree/master/elastic/doc/troubleshooting#too-many-active-shards-on-a-single-node).

#### Availability Zone Failure

Shards are distributed such that primary and replica shard always live in
different AZs if possible (e.g. on a healthy cluster) so we would still have
complete log data in one zone. If one AZ is down, the cluster would run with
degraded search performance and insufficient indexing performance and finally
switch indices to read-only mode because not all re-routed shards would fit into
the nodes of a single zone. Scaling up nodes in the other zone should mitigate
this, but re-routing and re-balancing would take some time.

#### Internal Cluster Failures

From our experience, the cluster can get into an unhealthy state through several
issues (see
[troubleshooting](https://gitlab.com/gitlab-com/runbooks/tree/master/elastic/doc/troubleshooting)
guide).

We are monitoring the health status of the [production
cluster](https://gitlab.com/gitlab-com/runbooks/blob/master/rules/elastic-clusters.yml#L24-33)
and the [monitoring
cluster](https://gitlab.com/gitlab-com/runbooks/blob/master/rules/elastic-clusters.yml#L44-53).

We will alert for [running out of disk
space](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/8548).

We are also [monitoring for ILM
errors](https://gitlab.com/gitlab-com/runbooks/blob/master/rules/elastic-clusters.yml#L4-23)
and will also monitor for [ILM being stopped
inadvertently](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/8972).

**TODO:** We will add a monitor for shard allocation failures.


### Performance Degradation

#### Degradation After Migration

Migrating to a new cluster is imposing the risk of degradation of indexing
performance and search latency. Low indexing performance could lead to logs not
being ingested in time and queuing up in PubSub. We confirmed sufficient
indexing performance by [testing, tuning and
benchmarking](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/8283).

**TODO:** [Benchmark search
latency](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/8959)

#### Degradation Because Of Shard Imbalance

There are a number of scenarios where significant changes to shard allocation
happen:

- nodes crashing, e.g. due to an OOM kill
- nodes being added, e.g. during resizing
- nodes being rotated, e.g. as part of maintenance work

This can lead to an imbalanced cluster, with the most active indices located on
a few nodes only. Symptoms are high CPU usage on just one (or a few) nodes and
the cluster falling behind in indexing logs from pubsubbeats. See
[troubleshooting](https://gitlab.com/gitlab-com/runbooks/tree/master/elastic/doc/troubleshooting#too-many-active-shards-on-a-single-node)
runbook.

We intend to alert on increases to indexing and search latency:
https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/8196

We already have alerts in place for unacked messages in PubSub subscriptions.
We try to limit this effect by controlling number of shards from the same index
per node:
https://gitlab.com/gitlab-com/runbooks/blob/master/elastic/managed-objects/lib/log_gprd_index_template.libsonnet#L22

**TODO:** We need to research this effect further and reach out to support.

### Scalability

We can scale the cluster when needed. We should be able to keep up with the
predicted 78% log growth rate for the next year and it mostly becomes a question
of costs if Elastic stays the right tool for the job. With migrating to the new
cluster we should be able to keep more logs for a longer time at lower costs
because of improvements in ILM and by switching to a hot-warm architecture.
Adding hot nodes increases indexing capacity. Adding warm nodes increases
retention capacity.

### Usability Changes

Migrating to the new version also brings several changes in the user interface.
While we don't know of any features that got lost, we are aware that there are
[UI usability
concerns](https://docs.google.com/document/d/1-U25f3e7MUXpTc8siWF9Clv5l6h-pgTkh8nry-zvlRE/edit).
Elastic is open for input from us and we hope they will address some of those
concerns.

## Rollback

We are running old and new cluster in parallel since a while. Finally switching
over to the new cluster will be done by announcing a date, reminding people to
transfer their visualizations/dashboards over to the new cluster until then (if
not already there) and switching a DNS record.  If serious issues show up, we
can easily reactivate the old cluster by switching the DNS entry back and making
sure that logs are still send to it.

## Data and Retention

We have approximately 1.4TiB logs daily coming in from pubsubbeat. Analysing the
index statistics shows that the index size in elastic is on average 3 times the
size of the logs from PubSub (depending on the index).

As of 17-01-2020 we have around 4TiB elastic storage usage per day (only PubSub
topics, excluding nginx and haproxy). That means for a 7 day retention we
consume around 28TiB storage. Adding nginx logs would increase that by
0.6TiB/day (15%), haproxy logs by 2.5TiB/day (63%).

The current cluster with a storage capacity of 55TiB has enough room for a 7 day
retention policy. We even could add the nginx logs without changes. Adding
haproxy logs with a 1 day retention period also could be considered. If indexing
rate would not be sufficient, we could add hot nodes. If we want to store more
data or keep it longer, we can add more warm nodes.

## Performance

There are two performance aspects to be considered: Indexing performance (to
make sure we are able to ingest all logs in real-time) and query performance
(delivering search results within an acceptable time).

### Indexing performance

We are indexing around 1.4TiB incoming logs per day or 17MiB/s on average with
peaks around 22Mb/s:
[prometheus](https://thanos-query.ops.gitlab.net/graph?g0.range_input=1d&g0.stacked=1&g0.max_source_resolution=0s&g0.expr=sum(stackdriver_pubsub_topic_pubsub_googleapis_com_topic_byte_cost%7Benv%3D%22gprd%22%7D%20%2F%2060)%20by%20(topic_id)&g0.tab=0).
The new cluster currently consumes 22.6TiB for 7 days worth of logs in 13.4
billion docs.  The pubsub volume for 7 days would be 1.4TiB * 7 = 9.8TiB. So
logs take around 2.3 times the amount of storage in Elastic than in pubsub.
13.4 billion logs in 7 days gives as an average indexing rate of **22k docs/s**.
And given that the pubsub bandwidth peak is 1.3 times higher than the average,
we can estimate the required indexing peak rate at **29k docs/s**.  To leave
some room for catching up with an eventual backlog, we should be aiming for an
indexing rate of at least **40k docs/s**.

[Benchmarking](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/8283)
showed that one elastic hot node is maxing out CPU at a primary indexing rate of
around 25k documents/s. A cluster with 8 hot nodes, 8 shards per index, a shard
size of 5GiB and one replica shard per primary, was able to reach a primary
indexing rate of up to 100k docs/s (falling down to 50k docs/s after a while
caused by a confirmed and fixed pubsubbeat bottleneck). Tuning parameters like
shard distribution should give even better results as we are not maxing out the
single hot nodes yet. This means that with optimal shard distribution the new
cluster has twice the required capacity and is able to keep up with the current
log ingestion without manual intervention and running it in parallel with the
old cluster since weeks is confirming it.

We discovered a bottleneck with PubSub: it is limited to an unchangeable
[StreamingPull Quota](https://cloud.google.com/pubsub/quotas) of 10MiB/s per
connection. As we were running one pubsubbeat per index we couldn't have a
single index ingest more than 10MiB/s. That is enough for normal operations (our
busiest log producers - rails, gitaly, workhorse - stay below 7MiB/s) but was
limiting our benchmarks and also made processing backlogs very slow. We got
[patches merged in
pubsubbeat](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/8856)
for enabling connection pooling which allowed us to establish multiple streaming
poll connections to PubSub. With that we were able to reach 25MiB/s per topic
with one pubsubbeat process, at which point we suspected the bottleneck was on
the cluster side.

### Query Performance

**TODO:** [Benchmark search
latency](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/8959) and
compare with old cluster

## Security

We are using Elastic Cloud as a hosted service. Security on OS and system level
is handled by elastic.io. The upgrade to ES 7.x brings us back to a version that
is supported by security updates and support.

The logs contain user data and thus need to be protected. Typically, we do not keep
logs longer than 7 days in Elastic. Encryption at rest is enabled by default in
Elastic Cloud.

Access to logs is available via Kibana, which is behind an [oauth
proxy](https://ops.gitlab.net/gitlab-com/gitlab-com-infrastructure/blob/master/environments/ops/main.tf#L91)
delegating authentication to [GCP
IAP](https://console.cloud.google.com/security/iap?authuser=1&project=gitlab-ops).


## Backup and Restore

There is no need to backup logs in Elastic as all logs are kept in Stackdriver
for 30 days and then moved into a GCS bucket with a retention of 1 year.

We are taking incremental daily snapshots in Elastic, which are kept for 7 days
and can be used to restore indices in a new cluster.

With the new cluster we started to keep nearly all
[configuration](https://gitlab.com/gitlab-com/runbooks/tree/master/elastic/managed-objects)
in version control and a CI job applying config changes. One of the things not
in version control are visualizations, because the mainly preferred way to
create them is via Kibana UI. We are considering to make regular backups of
visualizations:
https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/9055

Recreating a cluster should be possible by applying the configuration from
version control and adjusting the pubsubbeat config if necessary.

## Monitoring and Alerts

There was no monitoring of the old cluster besides elastic mails warning us of
low storage space. The way monitoring works for Elasticsearch is to configure
the cluster to forward x-pack monitoring metrics to another Elasticsearch
cluster. Clusters can monitor themselves or can send monitoring data to a
dedicated monitoring cluster.  Alerting is possible with the use of watches
which perform searches against Elasticsearch indices (in particular, against
indices with monitoring data).  There is no direct access to logs of the
clusters in Elastic Cloud. We can only access them through the Elastic Cloud web
UI.

We setup watches for important cluster functions (like errors in ILM) which are
checked by blackbox exporter for alerting.

To get metrics into prometheus we are
[planning](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/8947) to
deploy an
[elasticearchexporter](https://github.com/justwatchcom/elasticsearch_exporter).

Key SLIs to monitor:

* Query latency
* Query rate
* Availability
* Query error rate
* Delay of log visibility
* Completeness of logs

Relevant metrics can be viewed in the [logging
dashboard](https://dashboards.gitlab.net/d/USVj3qHmk/logging?orgId=1&refresh=30s)
in Grafana.

Issue for implementing monitoring and alerting for SLOs:
https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/9056

## Responsibility

* Infrastructure team
* subject matter experts:
  * @mwasilewski-gitlab
  * @igorwwwwwwwwwwwwwwwwwwww
  * @hphilipps

## Testing

We tested with staging logs and a dedicated new staging cluster. Also, we are
running the new prod cluster in parallel to the old cluster since a few weeks,
consuming the same logs. Indexing benchmarks have been used to test performance
with real logs.

## Readiness Review Participants

* @mwasilewski-gitlab
