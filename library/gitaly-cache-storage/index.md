# Blueprint: Cache-Accelerated Gitaly Storage

## Need

To help with GitLab.com finances, we need to reduce the cost of operating Gitaly.

## Approach

One of the major long-term scaling costs of Gitaly is storage. We currently allocate SSD-backed cloud storage filesystems.

In order to reduce cost, we can move to a SSD-cached and HDD-backed storage system. To implement this, we can use the [bcache block device caching storage feature][bcache] of Linux. Due to the simplicity of bcache, and the simplicity of implementation, this is by far the most boring solution.

The expected storage cost savings is between 70 and 75%, with total Gitaly operating cost savings of 65 to 70%. This approach is also designed to have minimal impact to overall performance.

This approach does not have any impact, for better or worse, on the durability or availability of Gitaly. It is only for cost savings while avoiding impact to complexity and  toil.

## Benefit

By using bcache, we can greatly reduce the cost of storage. It also has the least impact on our existing infrastructure and workflow.

* No changes to Gitaly required.

* No filesystem change. (we can continue to use ext4)

* No kernel changes required (bcache is in mainline Linux)

* The bcache layer is well tested and stable.

* No need to move inactive/archived projects to separate storage nodes.

* No blockers for other Gitaly changes, like Praefect.

* Can re-use existing repo management tools for repo moves and load balancing.

### IO Performance considerations

At the time of moving GitLab to GCP, IO limits on SSDs were much lower. Since the initial move, IOP quotas have greatly increased. GCP allocate IOP and bandwidth quotas for SSD and HDD persistent disks [based on the size and node CPU configuration][gcp disks]

With the current SSD allocation, we are allocated 60k read IOPs, 30k write IOPs, 1200MB/s read bandwidth, and 800MB/s write bandwidth.

During peak traffic times, we utilize 7k write IOPs and 10k read IOPs. Note, these are absolute peaks. The 99.9th percentile is below 2k.

![IOPs - Write](img/image_0.png)

![IOPS - Read](img/image_1.png)

For Bandwidth, we use approximately 150MB/s write, 100MB/s read.

![IO Bandwidth - 95th Percnetile](img/image_2.png)

In order to get equivalent performance from a caching layer, we only need to size for these peak values. GCP provides 30 read/write IOPs for SSDs per GB of size. This means in order to get 10k IOPs, we only need a 333GB SSD volume.

### Cache sizing considerations

Choosing the SSD cache to HDD storage ratio is important to the overall performance of a storage node. Having enough SSD space to absorb large writes without impact to colocated users is highly recommended.

GitLab currently limits projects to 10GiB. There is some possibility that individual writes may contain more than 10GiB due to the way Git/Gitaly works.

It is also important to make sure that heavy activity doesn’t cause the whole cache to blow out in a short period of time.

![Hourly Storage Activity - 95th Percentile](img/image_3.png)

Typical peak time hourly write traffic is under 50GiB. But we do see large write spikes.

Sizing the cache between 500GiB and 1TiB should allow for acceptable performance even under heavy load. The actual value needs to be tested 

### Storage considerations

The current Gitaly storage node specs are a GCP n1-standard-32 (32 CPU, 120GB memory) node with ~15.5TiB SSD filesystem.

The nodes are typically under-utilized for CPU by a sizeable safety margin.

In June 2020, the peak traffic 95th percentile CPU utilization for the production service is typically under 30-35%.

![CPU Utilization](img/image_4.png)

Expanding the storage capacity of each node to 30TiB should be possible. This will greatly improve our capacity by nearly two times without the need for additional nodes. We may also choose to reduce the number of nodes as part of the transition.

### Cache Mode

The cache operation has [several modes][cache modes]. One topic not covered here is the selection of local or persistent SSD for the cache. A local SSD may be an option if the cache is in a read-only mode. There is not currently enough data to determine which mode is going to operate best for a Gitaly workload. The current assumption is that Write-Back will be best based on the current direct to SSD mode of operation. However, Write-Around may be a viable option given that we can get sufficient IOPs from HDD storage with the current GCP 15k write IOPs per node.

#### Write-Back

Writes go to SSD first, then HDD is backfilled. Cache is read-write. This mode of operation is made possible by the durable persistent disks that we use today. SSD space is as durable as HDD space.

#### Write-Through

Writes must complete writing to both the cache and HDD backing store. Cache is read-only. This mode allows for immediate acceleration of new writes without a cache miss. But causes new writes that don’t need to be re-read to dirty the cache.

#### Write-Around

Writes are sent to HDD and not the cache. Cache is read-only. This mode allows new writes to not affect the cache. This has the benefit of allowing for large writes without impact to other read operations.

### Cost Reduction

The literal million dollar question is, how much will this approach save us.

* Current SSD storage is $2088.96/year per TiB.

* HDD storage is $491.52/year per TiB.

* Current SSD storage is 825TiB, with a total cost of $1.723M

* Selecting a 5% cache ratio would be $0.492M = **-71% saves $1.23M/year**

* Selecting a 2.5% cache ratio would be = **-74% saves $1.27M/year**

* Increasing the per-node disk to 30TiB and reducing the node count from 54 to 30 saves an additional **$0.224M**

**In total, it’s possible $1.5M would be saved annually, a 67% reduction in Gitaly compute and storage cost.**

## Competition

There are other options for tiered caching, but many require more system modifications or changing the underlying filesystem Gitaly currently writes to.

* bcachefs: The bcache developers also have a copy-on-write filesystem option.

    * Not in main-line Linux.

    * Still considered beta quality.

* ZFS

    * Not in main-line Linux.

    * Higher complexity.

    * More unknown unknowns for Gitaly developers.

* dm-cache

    * Tiered cache, objects are either cached or not.

### Object Storage

Object storage would provide a very large advantage over the current architecture. Since object storage allocation is pay for use, not pay for allocated space, an additional savings would be possible.

At the current use, we have 825TiB space allocated for non-marquee storage. But only 580TiB in use. Based on a yearly cost of $319/TiB for object storage, and a 5% SSD cache, we would see an 85% reduction in storage cost for gitaly.

In addition, object storage would provide multi-regional available storage, where today we have only single zone availability.

But without investment in Gitaly to support object storage, this is not possible.

[bcache]: https://en.wikipedia.org/wiki/Bcache
[gcp disks]: https://cloud.google.com/compute/docs/disks/performance#cpu_count_size
[cache modes]: https://shahriar.svbtle.com/Understanding-writethrough-writearound-and-writeback-caching-with-python
