---
layout: handbook-page-toc
title: "Blueprint: Postgresql Logical Replication
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Resources

Epic: [Postgresql Logical Replication](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/255)

## Idea or problem statement

Currently, in our PostgreSQL database cluster, we use the [streaming replication](https://www.postgresql.org/docs/11/protocol-replication.html) to replicate all the data to our secondaries. This mechanism is very efficient, keeping our secondaries with a replication lag under milliseconds. Basically, the primary server continues to send WAL data, and then, each secondary server replays locally the received data immediately.


### Limitations of Streaming replication 

The streaming replication is initialized for the full initial load using `pg_basebackup`. The following are some of the limitations of this procedure:  
-  primary and secondary hosts need to have the same PostgreSQL major version.
-  The database setup has to be equal. E.g, we couldn't start a secondary with checksums enabled or a different setup.

Considering the limitations mentioned above, we need to find alternatives to solve the following scenarios:
- Executing a PostgreSQL major release upgrade with near-zero downtime. 
- Replicating the data content for our database to a new database with a different setup, e.g, enabling checksums.


### Solutions

The solution chosen to solve these issues is the logical replication that is native on PostgreSQL, available since the major version 10. It is widely tested and supported by the community and meets our requirements, I would also like to mention that this process has some limitations and restrictions, which be explained in detail during the design document.
