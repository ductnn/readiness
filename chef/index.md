# Chef Server Readiness Review

## Table of Contents

<!-- vim-markdown-toc GitLab -->

* [Summary](#summary)
* [Documentation](#documentation)
* [Architecture](#architecture)
* [Operational Risk Assessment](#operational-risk-assessment)
* [Security](#security)
* [Performance](#performance)
* [Backup and Restore](#backup-and-restore)
* [Monitoring and Alerts](#monitoring-and-alerts)
* [Responsibility](#responsibility)
* [Testing](#testing)

<!-- vim-markdown-toc -->

## Summary

We need(ed) to migrate our Chef server from Digital Ocean to GCP for compliance reasons. As a
consequence, we are conducting a general review to ensure the service is managed in-line with the
most recent standards and has at least the minimum settings and tools to operate effectively.

Some initial primary outcomes for the migration will include

1. SSH access is secured behind a bastion host
1. Application and system logs are stored centrally, and searchable using our standard log
   management utilities
1. Service Level Indicators and target Service Level Objectives are defined
1. Application monitoring is in place for SLIs, with alerts for basic SLOs
1. Application backups are configured and tested
1. Enabling security scans via https://teneble.io

The epic for the migration work is https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/138, and
the [server migration has already been completed](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/2711).
This exercise is being used to ensure that we do not miss any near-term follow-up work, and that we
capture any long-term engineering effort required to stabilize the service, effectively document the
current state, and manage operations going forward.

## Documentation

- [Chef Runbooks](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/config_management)

## Architecture

- [Production Architecture](https://about.gitlab.com/handbook/engineering/infrastructure/production/architecture/#chef-architecture)

## Operational Risk Assessment

## Security

## Performance

## Backup and Restore

A disk snapshot of the Chef server data drive is made every four hours and
available for restore for up to 14 days. The underlying infrastructure is
created and managed by Terraform and can be recreated if required. The Chef
service and virtual machine configuration is managed by Chef cookbooks that
can be used to bootstrap a replacement virtual machine if needed. And the
chef repo project can be used to update a restored server or replace missing
roles, environments, nodes, and cookbooks.

* [Ops Snapshots Management](https://ops.gitlab.net/gitlab-com/gl-infra/gitlab-restore/gitlab-ops-snapshots)
* [Terraform Infrastructure](https://ops.gitlab.net/gitlab-com/gitlab-com-infrastructure)
* [GitLab Chef Server Cookbook](https://gitlab.com/gitlab-cookbooks/gitlab-chef-server)
* [Chef Server Content and CI Project](https://ops.gitlab.net/gitlab-cookbooks/chef-repo)

## Monitoring and Alerts

## Responsibility

As a service, the Chef Infrastructure server is owned by @gitlab-com/gl-infra/sre-coreinfra. Within
that team, the primary Subject Matter Experts are @craig and @cmcfarland.

## Testing
